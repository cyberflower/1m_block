#include <string.h>
#include <string>

const int SZ=(int)2e7+5;

int trie[SZ][30];
struct Trie{
  int tot;
  Trie();
  void update(string &x);
  bool find(string &x);
};
