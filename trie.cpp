#include "trie.h"

using namespace std;

struct Trie{
  int tot;
  Trie(){
    memset(trie,0,sizeof(trie));
    tot=0;
  }
  void update(string &x){
    int p=0;
    int sz=(int)x.size();
    for(int i=0;i<sz;i++){
      if(!trie[p][x[i]-'a']) trie[p][x[i]-'a']=++tot;
      p=trie[p][x[i]-'a'];
    }
  }
  bool find(string &x){
    int p=0;
    int sz=(int)x.size();
    for(int i=0;i<sz;i++){
      if(!trie[p][x[i]-'a']) return false;
      p=trie[p][x[i]-'a'];
    }
    return true;
  }
};
