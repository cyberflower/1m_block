import csv
import os

if __name__=='__main__':
    tot=0
    reader=[]
    with open("top-1m.csv","r",newline='') as fp_read:
        reader=csv.reader(fp_read,delimiter=' ', quotechar='|')
        if os.path.isfile("top-1m.txt"):
            os.remove("top-1m.txt")
        fp_write=open("top-1m.txt","a")
        for row in reader:
            str=(row[0].split(','))[1]
            tot+=len(str)
            if(len(str)==0): continue;
            fp_write.write(str+'\n')
    print(tot)
