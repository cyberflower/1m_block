all : clean 1m_block

1m_block: 1m_block.o
	g++ -g -o 1m_block 1m_block.o -lnetfilter_queue

1m_block.o:
	g++ -std=c++14 -g -c -o 1m_block.o 1m_block.cpp

clean:
	rm -rf *.o
