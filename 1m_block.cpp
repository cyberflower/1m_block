#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <string>

#include <libnetfilter_queue/libnetfilter_queue.h>
#include <libnet.h>

//#include "trie.h"

using namespace std;

const int SZ=1e7+1e6;	// check total length of urls with python
const char point_char='z'+1;
const char end_char='z'+2;

int trie[SZ][30];
int tot;

void TRIE_update(string &x){
  int p=0;
  int sz=(int)x.size();
  for(int i=0;i<sz;i++){
    int xx=(x[i]=='.'?point_char:x[i])-'a';
    if(xx<0 || xx>28) return;
    if(!trie[p][xx]) trie[p][xx]=++tot;
    p=trie[p][xx];
  }
}

bool TRIE_find(char *start, int sz){
  int p=0;
  for(int i=0;i<sz;i++){
    int xx=(*(start+i)=='.'?point_char:*(start+i))-'a';
    if(xx<0 || xx>28) return false;
    if(!trie[p][xx]) return false;
    p=trie[p][xx];
  }
  if(!trie[p][end_char-'a']) return false;
  return true;
}

void usage(){
	printf("syntax : 1m_block <block>\n");
	printf("sample : 1m_block top-1m.txt\n");
}

void find_url(char *data, int data_len, int *start, int *len){
	char host[10]="Host: ";
	for(int i=0;i<data_len-6;i++){
		if(memcmp(data+i,host,strlen(host))==0){
			*start=i+6;
			break;
		}
	}
	*len=0;
	for(int i=(*start);i<data_len-2;i++){
		if(memcmp(data+i,"\x0d\x0a",2)==0){
			*len=(i-(*start));
			break;
		}
	}
}

bool isblock(unsigned char *data, int data_len){
	char start_tcp_data[6][10]={"GET", "POST", "HEAD", "PUT", "DELETE", "OPTIONS"};
	struct libnet_ipv4_hdr *ipv4_hdr=(struct libnet_ipv4_hdr*)data;
	struct libnet_tcp_hdr *tcp_hdr=(struct libnet_tcp_hdr*)(data+ipv4_hdr->ip_hl*4);
	char *app_data=(char *)(data+ipv4_hdr->ip_hl*4+tcp_hdr->th_off*4);
	if(ipv4_hdr->ip_v==0x4 && ipv4_hdr->ip_p==0x6){
		for(int i=0;i<6;i++){
			if(memcmp(start_tcp_data[i],app_data,strlen(start_tcp_data[i]))==0){
				int start_url=-1, len=-1;
				find_url(app_data,data_len,&start_url,&len);
				if(start_url!=-1 && TRIE_find(app_data+start_url,len)) return true;
				break;
			}
		}
		return false;
	}
	return false;
}

/* returns packet id */
static u_int32_t print_pkt (struct nfq_data *tb, bool *flg){
	int id = 0;
	struct nfqnl_msg_packet_hdr *ph;
	struct nfqnl_msg_packet_hw *hwph;
	u_int32_t mark,ifi;
	int ret;
	unsigned char *data;

	ph = nfq_get_msg_packet_hdr(tb);
	if (ph)	id = ntohl(ph->packet_id);

	hwph = nfq_get_packet_hw(tb);
	if (hwph) {
		int i, hlen = ntohs(hwph->hw_addrlen);
	}

	mark = nfq_get_nfmark(tb);
	ifi = nfq_get_indev(tb);
	ifi = nfq_get_outdev(tb);
	ifi = nfq_get_physindev(tb);
	ifi = nfq_get_physoutdev(tb);
	ret = nfq_get_payload(tb, &data);
	if (ret >= 0)		*flg=isblock(data, ret);
	return id;
}


static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
	      struct nfq_data *nfa, void *data)
{
	bool flg=true;
	u_int32_t id = print_pkt(nfa, &flg);
	if(flg) return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
	else		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
}

int main(int argc, char **argv){
	if(argc!=2){
		usage();
		exit(-1);
	}

	struct nfq_handle *h;
	struct nfq_q_handle *qh;
	struct nfnl_handle *nh;
	int fd;
	int rv;
	char buf[4096] __attribute__ ((aligned));

	ifstream inFile(argv[1]);

	//block_urls=Trie();
	while(!inFile.eof()){
		string url; getline(inFile,url);
		url+=end_char;	// push end character
		TRIE_update(url);
	}

	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	qh = nfq_create_queue(h,  0, &cb, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	fd = nfq_fd(h);

	for (;;) {
		if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
			nfq_handle_packet(h, buf, rv);
			continue;
		}
		/* if your application is too slow to digest the packets that
		 * are sent from kernel-space, the socket buffer that we use
		 * to enqueue packets may fill up returning ENOBUFS. Depending
		 * on your application, this error may be ignored. nfq_nlmsg_verdict_putPlease, see
		 * the doxygen documentation of this library on how to improve
		 * this situation.
		 */
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}

	//printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	//printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif

	//printf("closing library handle\n");
	nfq_close(h);

	exit(0);
}
